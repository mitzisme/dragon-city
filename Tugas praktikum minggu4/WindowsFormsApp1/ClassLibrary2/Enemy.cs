﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class Enemy
    {
        private int _x;

        public int x
        {
            get { return _x; }
            set { _x = value; }
        }

        private int _y;

        public int y
        {
            get { return _y; }
            set { _y = value; }
        }

        private int _dy;

        public int dy
        {
            get { return _dy; }
            set { _dy = value; }
        }

        private int _key;

        public int key
        {
            get { return _key; }
            set { _key = value; }
        }

        private Boolean _status;

        public Boolean status
        {
            get { return _status; }
            set { _status = value; }
        }

        private Boolean _tambahspeed;

        public Boolean tambahspeed
        {
            get { return _tambahspeed; }
            set { _tambahspeed = value; }
        }


        public Enemy(int x,int key)
        {
            _x = x;
            _key = key;
            _y = 350;
            _dy = 10;
            status = true;
        }

        public void gerak()
        {
            _y -= dy;
        }

        public void kena_pukul()
        {
            status = false;
        }
        public void tambah_speed()
        {
            if (tambahspeed == false)
            {
                _dy = 20;
                tambahspeed = true;
            }
        }
    }
}
