﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary2;
using System.Drawing;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        int[] angka_highscore = new int[5];
        Random r = new Random();


        Graphics graph;
        Font f;
        Pen p = new Pen(Color.Black);

        int angka_rand = 0;
        int lebar_map = 400;
        int tinggi_map = 500;
        int x_map = 300;
        int y_map = 0;
        int Life=0;
        int Score=0;

        int ctr_pukulkiri=0;
        int ctr_pukulkanan=0;
        int ctr_tendangtengah = 0;
        int ctr_spawn_musuh = 0;
        int ctr_gerak_musuh = 0;
        int ctr_animasi_kena = 0;
        int ctr_hilangkan = 0;

        Boolean tarung = false;
        Boolean main_menu=true;
        Boolean highscore = false;
        Boolean tendang_tengah = false;
        Boolean pukul_kiri = false;
        Boolean pukul_kanan = false;
        Boolean kena_enemy = false;

        Image stage= Image.FromFile("Stage.png");

        Image berdiri = Image.FromFile("berdiri.png");

        Image down_kiri = Image.FromFile("down_enemy_kiri.png");
        Image down_tengah = Image.FromFile("down_enemy_tengah.png");
        Image down_kanan = Image.FromFile("down_enemy_kanan.png");

        Image[] kiri = new Image[3];
        Image[] tengah = new Image[3];
        Image[] kanan = new Image[3];
        Image[] kena = new Image[6];

        Image[] musuh1 = new Image[3];
        Image[] musuh2 = new Image[3];

        List<Enemy> lis_enemy = new List<Enemy>();

        public Form1()
        {
            InitializeComponent();
            this.Width = 1000;
            this.Height = 1000;

            kiri[0]= Image.FromFile("kiri0.png");
            kiri[1] = Image.FromFile("kiri1.png");
            kiri[2] = Image.FromFile("kiri2.png");

            tengah[0] = Image.FromFile("tengah0.png");
            tengah[1] = Image.FromFile("tengah1.png");
            tengah[2] = Image.FromFile("tengah2.png");

            kanan[0] = Image.FromFile("kanan0.png");
            kanan[1] = Image.FromFile("kanan1.png");
            kanan[2] = Image.FromFile("kanan2.png");

            musuh1[0] = Image.FromFile("musuh10.png");
            musuh1[1] = Image.FromFile("musuh11.png");
            musuh1[2] = Image.FromFile("musuh12.png");

            musuh2[0] = Image.FromFile("musuh20.png");
            musuh2[1] = Image.FromFile("musuh21.png");
            musuh2[2] = Image.FromFile("musuh22.png");

            kena[0] = Image.FromFile("kena1.png");
            kena[1] = Image.FromFile("kena2.png");
            kena[2] = Image.FromFile("kena1.png");
            kena[3] = Image.FromFile("kena2.png");
            kena[4] = Image.FromFile("kena1.png");

            for(int i = 0; i < 5; i++)
            {
                angka_highscore[i] = 0;
            }

            this.DoubleBuffered = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            graph = e.Graphics;
            f = new Font("Arial", 14, FontStyle.Bold);

            Brush b = new SolidBrush(Color.Yellow);
            Brush Tulisan = new SolidBrush(Color.Black);


            if (main_menu == true)
            {
                graph.DrawRectangle(p,100,100,50,30);
                
                graph.DrawString("Play", f, Tulisan, 100, 100);

                graph.DrawRectangle(p, 100, 150, 100, 30);

                graph.DrawString("Highscore", f, Tulisan, 100, 150);

                graph.DrawRectangle(p, 100, 200, 50, 30);

                graph.DrawString("Quit", f, Tulisan, 100, 200);
            }
            else if(tarung==true)
            {
                graph.DrawImage(stage, x_map, y_map, lebar_map, tinggi_map);

                graph.DrawString("Life :"+Life.ToString(), f, Tulisan, 100, 200);

                graph.DrawString("Score :"+Score.ToString(), f, Tulisan, 100, 300);

                if (pukul_kiri == true)
                {
                    graph.DrawImage(kiri[ctr_pukulkiri], 460, 50, 100, 150);
                }
                else if(pukul_kanan==true)
                {
                    graph.DrawImage(kanan[ctr_pukulkanan], 460, 50, 100, 150);
                }
                else if (tendang_tengah == true)
                {
                    graph.DrawImage(tengah[ctr_tendangtengah], 460, 50, 100, 150);
                }
                else if (kena_enemy == true)
                {
                    graph.DrawImage(kena[ctr_animasi_kena], 460, 50, 100, 150);
                }
                else
                {
                    graph.DrawImage(berdiri, 450, 50, 100, 150);
                }

                if (kena_enemy != true)
                {
                    for (int i = 0; i < lis_enemy.Count(); i++)
                    {
                        if (lis_enemy[i].key == 1 || lis_enemy[i].key == 3)
                        {
                            if (lis_enemy[i].status != false)
                            {
                                graph.DrawImage(musuh1[ctr_gerak_musuh], lis_enemy[i].x, lis_enemy[i].y, 100, 150);
                            }
                            else
                            {
                                if (lis_enemy[i].key == 1)
                                {
                                    graph.DrawImage(down_kiri, lis_enemy[i].x, lis_enemy[i].y, 100, 150);
                                }
                                if (lis_enemy[i].key == 3)
                                {
                                    graph.DrawImage(down_kanan, lis_enemy[i].x, lis_enemy[i].y, 100, 150);
                                }
                            }
                        }
                        if (lis_enemy[i].key == 2)
                        {
                            if (lis_enemy[i].status != false)
                            {
                                graph.DrawImage(musuh2[ctr_gerak_musuh], lis_enemy[i].x, lis_enemy[i].y, 100, 150);
                            }
                            else
                            {
                                graph.DrawImage(down_tengah, lis_enemy[i].x, lis_enemy[i].y, 100, 150);
                            }
                        }

                    }
                }
            }
            else if (highscore == true)
            {
                graph.DrawString("HIGH SCORE", f, Tulisan, 100, 100);

                graph.DrawString("1 :"+angka_highscore[0].ToString(), f, Tulisan, 100, 200);

                graph.DrawString("2 :" + angka_highscore[1].ToString(), f, Tulisan, 100, 300);

                graph.DrawString("3 :" + angka_highscore[2].ToString(), f, Tulisan, 100, 400);

                graph.DrawString("4 :" + angka_highscore[3].ToString(), f, Tulisan, 100, 500);

                graph.DrawString("5 :" + angka_highscore[4].ToString(), f, Tulisan, 100, 600);

                graph.DrawString("Back " , f, Tulisan, 300, 100);
            }

        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            int tinggi = 100;
            if (tarung == false)
            {
                if (main_menu == true)
                {
                    Rectangle mouse = new Rectangle(e.X, e.Y, 5, 5);
                    for (int i = 0; i < 3; i++)
                    {
                        if (i != 2)
                        {
                            if (i == 0)
                            {
                                Rectangle kotak = new Rectangle(100, 100, 50, 30);
                                if (kotak.IntersectsWith(mouse))
                                {
                                    main_menu = false;
                                    tarung = true;
                                    ctr_pukulkiri = 0;
                                    ctr_pukulkanan = 0;
                                    ctr_tendangtengah = 0;
                                    ctr_spawn_musuh = 0;
                                    ctr_gerak_musuh = 0;
                                    ctr_animasi_kena = 0;
                                    ctr_hilangkan = 0;
                                    tendang_tengah = false;
                                    pukul_kiri = false;
                                    pukul_kanan = false;
                                    kena_enemy = false;
                                    timer1.Start();
                                    Life = 5;
                                    Score = 0;
                                }
                            }
                            else
                            {
                                Rectangle kotak = new Rectangle(100, 150, 100, 30);
                                if (kotak.IntersectsWith(mouse))
                                {
                                    main_menu = false;
                                    highscore = true;
                                }
                            }
                        }
                        else
                        {
                            Rectangle kotak = new Rectangle(100, 200, 100, 50);
                            if (kotak.IntersectsWith(mouse))
                            {

                                this.Close();
                            }
                        }
                    }

                    Invalidate();
                }
                else if (highscore == true)
                {
                    Rectangle mouse = new Rectangle(e.X, e.Y, 5, 5);
                    Rectangle kotak = new Rectangle(300, 100, 100, 30);
                    if (kotak.IntersectsWith(mouse))
                    {
                        highscore = false;
                        main_menu = true;
                    }
                }
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "A")
            {
                pukul_kiri = true;
                ctr_pukulkiri = 0;
            }

            if (e.KeyCode.ToString() == "D")
            {
                pukul_kanan = true;
                ctr_pukulkanan = 0;
            }
            if (e.KeyCode.ToString() == "S")
            {
                tendang_tengah = true;
                ctr_tendangtengah = 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int jumlah_musuh = r.Next(3)+1;

            if (kena_enemy == false)
            {
                ctr_spawn_musuh++;
                ctr_gerak_musuh++;
                ctr_hilangkan++;

                if (Score > 10)
                {
                    for (int i = 0; i < lis_enemy.Count(); i++)
                    {
                        lis_enemy[i].tambah_speed();
                    }
                }

                if (pukul_kiri == true || pukul_kanan == true || tendang_tengah == true)
                {
                    timer2.Start();
                    for (int i = lis_enemy.Count() - 1; i >= 0; i--)
                    {
                        Rectangle enemy_place = new Rectangle(lis_enemy[i].x, lis_enemy[i].y, 100, 150);
                        Rectangle hitbox1 = new Rectangle(300, 200, 100, 100);

                        Rectangle hitbox2 = new Rectangle(450, 200, 100, 100);

                        Rectangle hitbox3 = new Rectangle(600, 200, 100, 100);

                        if (lis_enemy[i].key == 1 && enemy_place.IntersectsWith(hitbox1) && pukul_kiri == true)
                        {
                            if (lis_enemy[i].status == true)
                            {
                                lis_enemy[i].kena_pukul();
                                punch();
                                Score += 1;
                            }
                        }
                        else if (lis_enemy[i].key == 2 && enemy_place.IntersectsWith(hitbox2) && tendang_tengah == true)
                        {
                            if (lis_enemy[i].status == true)
                            {
                                lis_enemy[i].kena_pukul();
                                punch();
                                Score++;
                            }
                        }
                        else if (lis_enemy[i].key == 3 && enemy_place.IntersectsWith(hitbox3) && pukul_kanan == true)
                        {
                            if (lis_enemy[i].status == true) { 
                            lis_enemy[i].kena_pukul();
                                punch();
                                Score++;
                            }
                        }
                    }
                }


                if (ctr_pukulkiri > 2)
                {
                    timer2.Stop();
                    pukul_kiri = false;
                    ctr_pukulkiri = 0;
                }
                if (ctr_pukulkanan > 2)
                {
                    timer2.Stop();
                    pukul_kanan = false;
                    ctr_pukulkanan = 0;
                }
                if (ctr_tendangtengah > 2)
                {
                    timer2.Stop();
                    tendang_tengah = false;
                    ctr_tendangtengah = 0;
                }

                if (ctr_gerak_musuh == 3)
                {
                    ctr_gerak_musuh = 0;
                }

                if (ctr_hilangkan == 6)
                {
                    for (int i = lis_enemy.Count() - 1; i >= 0; i--)
                    {
                        if (lis_enemy[i].status == false)
                        {
                            lis_enemy.RemoveAt(i);
                        }
                    }
                    ctr_hilangkan = 0;
                }

                for (int i = lis_enemy.Count() - 1; i >= 0; i--)
                {
                    if (kena_enemy != true)
                    {
                        lis_enemy[i].gerak();
                        if (lis_enemy[i].y <= 50)
                        {
                            lis_enemy.RemoveAt(i);
                            Life -= 1;
                            kena_enemy = true;
                            lis_enemy.Clear();
                        }
                    }
                }

                if (ctr_spawn_musuh == 20)
                {
                    if (kena_enemy != true)
                    {
                        for (int i = 0; i < jumlah_musuh; i++)
                        {
                            if (i == 0)
                            {
                                lis_enemy.Add(new Enemy(300, 1));
                            }
                            if (i == 1)
                            {
                                lis_enemy.Add(new Enemy(450, 2));
                            }
                            if (i == 2)
                            {
                                lis_enemy.Add(new Enemy(600, 3));
                            }
                        }

                        ctr_spawn_musuh = 0;
                    }
                }
                Invalidate();
            }
            else
            {
                timer2.Start();

                if (ctr_animasi_kena == 5)
                {
                    timer2.Stop();

                    if (Life == 0)
                    {
                        for(int i = 5; i >= 1; i--)
                        {
                            if (i == 5)
                            {
                                if (Score > angka_highscore[i - 1])
                                {
                                    angka_highscore[i-1] = Score;
                                }
                            }
                            else
                            {
                                if (angka_highscore[i] > angka_highscore[i - 1])
                                {
                                    int temp = angka_highscore[i - 1];
                                    angka_highscore[i - 1] = angka_highscore[i];
                                    angka_highscore[i] = temp;
                                }
                            }
                        }
                        main_menu = true;
                        tarung = false;
                        lis_enemy.Clear();
                        timer1.Stop();
                    }
                    else
                    {
                        kena_enemy = false;
                        ctr_gerak_musuh = 0;
                        ctr_animasi_kena = 0;
                        ctr_spawn_musuh=0;
                        ctr_gerak_musuh=0;
                        ctr_hilangkan=0;
                    }
                }
                Invalidate();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (pukul_kiri==true)
            {
                ctr_pukulkiri++;
            }
            if (pukul_kanan==true)
            {
                ctr_pukulkanan++;
            }
            if (tendang_tengah==true)
            {
                ctr_tendangtengah++;
            }
            if (kena_enemy == true)
            {
                ctr_animasi_kena++;
            }
        }

        public void punch()
        {
            String sound = "punch.wav";
            var sounds = new System.Media.SoundPlayer(sound);
            sounds.Play();
        }
    }
}
